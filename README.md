# Chapter 01: Introducing the Cowboy Web Server

## Run server

To run the server go to `cowboy_example` directory and execute following command:
```shell
mix run --no-halt
```

Server will be available at [http://localhost:4040](http://localhost:4040).

